# Colors
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

PROJECT=${1-myproject}

echo "${green}>>> Remove .venv${reset}"
rm -rf .venv

echo "${green}>>> Creating virtualenv${reset}"
virtualenv -p python3 .venv
echo "${green}>>> .venv is created.${reset}"

# active
sleep 2
echo "${green}>>> activate the .venv.${reset}"
source .venv/bin/activate
PS1="(`basename \"$VIRTUAL_ENV\"`)\e[1;34m:/\W\033[00m$ "
sleep 2

# installdjango
echo "${green}>>> Installing the requirements${reset}"
pip install -r requirements.txt
sleep 2

# npm install 
echo "${green}>>> Installing the dependencie frontend${reset}"
npm install
npm start

sleep 2
echo "${green}>>> Create .env${reset}"
rm .env

sleep 2
echo "${green}>>> Create .env${reset}"
touch .env 
echo "DEBUG=True" >> .env
echo "TEMPLATE_DEBUG=%(DEBUG)s" >>  .env
echo "SECRET_KEY=ARANDOMSECRETKEY" >> .env


sleep 2
# run
echo "${green}>>> Run migrate${reset}"
python manage.py migrate

echo "${green}>>> Create super user ${reset}"
python manage.py createsuperuser

sleep 2
echo "${green}>>> Done${reset}"

python manage.py runserver