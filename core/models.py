from django.db import models
from django.utils import timezone


class Participant(models.Model):
    name = models.CharField(max_length=200, unique=True)
    image = models.ImageField(upload_to = 'photos')
    walled = models.BooleanField(default=False)
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return "/media/%s" %self.image

class Wall(models.Model):
    number = models.IntegerField(unique=True)
    participant = models.ManyToManyField(Participant, through='Vote')
    created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '%s ' % (self.number)


class Vote(models.Model):
    participant = models.ForeignKey(Participant, related_name='vote' ,on_delete=models.CASCADE)
    wall = models.ForeignKey(Wall, on_delete=models.CASCADE)
    created_by = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '%s - Wall : %s ' % (self.participant, self.wall)