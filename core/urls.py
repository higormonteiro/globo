from django.urls import path

from . import views
#from . import viewsets

urlpatterns = [
    #path('votes/', viewsets.VoteViewSet.as_view()),
    path('participants/', views.ParticipantListCreatView.as_view()),
]
