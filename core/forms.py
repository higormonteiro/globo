from django import forms
from django.utils import timezone
from .models import Participant, Wall, Vote


class ParticipantForms(forms.ModelForm):
    participant = forms.ModelChoiceField(
        queryset=Participant.objects.filter(walled=True)
        )

    class Meta:
        model = Vote
        widgets = {'wall': forms.HiddenInput()}
        fields = ('participant', 'wall',)
    
    def __init__(self, *args, **kwargs):
        super(ParticipantForms, self).__init__(*args, **kwargs)
        last_wall = Wall.objects.only('id').latest('created')
        self.fields['wall'].initial = last_wall

