from rest_framework import serializers
from rest_framework.generics import ListCreateAPIView
from .models import Vote, Participant, Wall


class ParticipantSerializer(serializers.ModelSerializer):

    class Meta:
        model = Participant
        fields = ['id', 'name', 'image']


class VoteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vote
        fields = ['wall', 'participant']


class AllVotesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vote
        fields = '__all__'