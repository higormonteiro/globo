from rest_framework import generics
from .models import Vote, Participant
from .serializers import ParticipantSerializer, VoteSerializer


class VoteListCreatView(generics.ListCreateAPIView):
    queryset = Vote.objects.all()
    serializer_class = VoteSerializer


class ParticipantListCreatView(generics.ListCreateAPIView):
    queryset = Participant.objects.all()
    serializer_class = ParticipantSerializer