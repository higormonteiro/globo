from django.contrib import admin
from django.contrib import messages
from .models import Participant, Wall, Vote


admin.site.register(Participant)
admin.site.register(Wall)
admin.site.register(Vote)
