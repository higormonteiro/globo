from rest_framework import viewsets
from rest_framework.response import Response
from django.db.models import Count
from .models import Vote, Participant, Wall
from .serializers import VoteSerializer, ParticipantSerializer, AllVotesSerializer


class VoteViewSet(viewsets.ModelViewSet):
    serializer_class = VoteSerializer
    queryset = Vote.objects.all()
   

class ParticipantViewSet(viewsets.ModelViewSet):
    serializer_class = ParticipantSerializer
    queryset = Participant.objects.filter(walled=True)


class AllVotesViewSet(viewsets.ModelViewSet):

    queryset = Vote.objects.all()
    serializer_class = AllVotesSerializer

    def list(self, request, format=None):
        last_wall = Wall.objects.values_list('number', flat=True).last()
        

        total_votes = Vote.objects.filter(wall=last_wall, participant__walled=True)\
            .values('wall')\
            .annotate(total_votes=Count('participant')
            )
        
        total = total_votes[0].get('total_votes')
        total_hora = total/60
        print(total_hora)

        votes_count = Vote.objects.filter(
            wall=last_wall, participant__walled=True)\
            .values('participant__name', 'participant', 'participant__image')\
            .annotate(votes_count=(Count('participant')*100)/total
            )
        

        qs = {'participants': votes_count, 'total_votes': total_votes}
        return Response(qs)