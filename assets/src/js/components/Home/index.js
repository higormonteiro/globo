import React, { Component } from 'react';
import Participant from '../Participant'
import Header from '../../components/Header';
import Modal from '../../components/Modal';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
        participants: [],
    };
  }
    render() {
      return (
          <div>
            <Header />
              <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
                  <h1 class="display-4">BIG BROTHER BRASIL</h1>
                      <p class="lead">Votações abertas</p>
              </div>
               <Participant />
               <Modal />
            </div>
      );
    }
  }

            
export default Home;

