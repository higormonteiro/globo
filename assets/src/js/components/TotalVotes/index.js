import React, { Component } from 'react';
import { Card, CardImg, Progress, CardBody,
    CardTitle, CardSubtitle, Col, Table, Row, Container } from 'reactstrap';
import Header from '../Header';

const API = '/api/all_votes/?format=json';

class TotalVotes extends Component {
  constructor(props) {
    super(props);

    this.state = {
        participants: [],
        votes: [],

    };
  }

  componentDidMount(e) {
    fetch(API)
      .then(response => response.json())
      .then(data => this.setState({ participants: data.participants }));
    
      fetch(API)
      .then(response => response.json())
      .then(data => this.setState({ votes: data.total_votes }));
    }
    render() {
        const { participants } = this.state;
        const { votes } = this.state;
      return (
        <div >
            <Header/>
            <Container>
                <h1>Resultado Parcial</h1>
                {votes.map(vote => 
                    <h3> Total de  {vote.total_votes} votos</h3>
                    )}

              <Row>
                  {participants.map(participant => 
                    <Col xs="4">
                        <Card id={participant.participant}>
                          <CardImg top width="100%" src={`/media/${participant.participant__image}` } alt={participant.participant__name} />
                          <CardBody>
                            <CardTitle>{participant.participant__name}</CardTitle>
                            <CardSubtitle>Votos : {participant.votes_count} %</CardSubtitle>
                            < Progress value={participant.votes_count} />
                          </CardBody>
                        </Card>
                    </Col>
                    
                    )}
                </Row>
            </Container>
        </div>

            
      );
    }
  }

            
export default TotalVotes;

