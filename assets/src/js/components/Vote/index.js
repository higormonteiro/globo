import React, { Component } from 'react';
import { Button } from 'reactstrap';

const API = '/api/votes/?format=json';

class Vote extends Component {
  constructor(props) {
    super(props);

    this.state = {
        vote: [],
    };
  }

  componentDidMount(e) {
    fetch(API)
      .then(response => response.json())
      .then(data => this.setState({ vote: data }));
    }
    render() {
        const { vote } = this.state;

      return (
          <div>
            <Button color="primary" size="lg" active>Envie seu voto</Button>{' '}
          </div>
      );
    }
  }

            
export default Vote;

