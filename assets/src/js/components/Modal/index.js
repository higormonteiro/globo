import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,
 Container, Col } from 'reactstrap';
import Participant from '../Participant';
import Vote from '../Vote';


class ModalExample extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  participants() {
    this.setState({
      participants: !this.state.participants
    });
  }

  render() {

    return (
      <div class="text-center">
        <Button color="primary" onClick={this.toggle}>VOTE AGORA</Button>
        <Modal  size="lg" isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader  toggle={this.toggle}>QUEM DEVE SER <b>ELIMINADO?</b></ModalHeader>
          <ModalBody >
          <Container>
            
            <Participant />
              
          </Container>
          </ModalBody>
          <ModalFooter>
              <Vote />
          </ModalFooter>
        </Modal>
      </div>
    );
    }
}

export default ModalExample;