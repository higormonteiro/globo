import React, { Component } from 'react';
import { Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Row, Col, Container } from 'reactstrap';

const API = '/api/participant/?format=json';

class Participant extends Component {
  constructor(props) {
    super(props);

    this.state = {
        participants: [],
    };
  }

  componentDidMount(e) {
    fetch(API)
      .then(response => response.json())
      .then(data => this.setState({ participants: data }));
    }
    render() {
        const { participants } = this.state;


      return (
          <div >
            <Container>
              <Row>
                  {participants.map(participant => 
                    <Col xs="4">
                        <Card id={participant.id}>
                          <CardImg top width="100%" src={participant.image}  alt={participant.name} />
                          <CardBody>
                            <CardTitle>{participant.name}</CardTitle>
                            <CardSubtitle>Card subtitle</CardSubtitle>
                            <CardText>....</CardText>
                          </CardBody>
                        </Card>
                    </Col>
                  )}
                </Row>
            </Container>
            </div>
      );
    }
  }

            
export default Participant;

