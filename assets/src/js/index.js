import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/index.css';

ReactDOM.render(<App />, document.getElementById('react-app'));