import React from 'react';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import Home from '../../components/Home';
import Modal from '../../components/Modal';
import TotalVotes from  '../../components/TotalVotes';

class App extends React.Component {
  render () {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/votes" component={TotalVotes} />
        </Switch>
    </BrowserRouter>
    )
  }
}

export default App;