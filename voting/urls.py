from django.urls import path, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

from rest_framework import routers
from core.viewsets import VoteViewSet, ParticipantViewSet, AllVotesViewSet

router = routers.DefaultRouter()
router.register(r'votes', VoteViewSet)
router.register(r'participant', ParticipantViewSet)
router.register(r'all_votes', AllVotesViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('', TemplateView.as_view(template_name='front-end/index.html')),
    path('votes/', TemplateView.as_view(template_name='front-end/votes.html')),
]

if settings.DEBUG:
    urlpatterns += static(
            settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)